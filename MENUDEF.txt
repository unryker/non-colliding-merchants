AddOptionMenu "DeathstriderMenuAddons"
{
	Submenu "Less Annoying Merchants", "DS_UR_NCM_Options"
}

OptionMenu "DS_UR_NCM_Options"
{
	Title "Less Annoying Merchants Options"
	StaticText ""
	
	Option "Less Annoying Merchants", "ds_ur_thrumerchants_enabled", "OnOff"
	StaticText "Toggles the mod on or off.", "White"
	StaticText "Only relevant when the map is changed.", "White"
}