### Notes
---
- An addon for [Deathstrider.](https://gitlab.com/accensi/deathstrider)
- Merchants no longer collide with players.
- Merchants no longer take damage when Ohana is active (they will still perish if they're separated from you for too long).
- If loading with an existing saved game, this addon won't initialize until next map.
- Compatible with all merchant skins (even most custom ones).